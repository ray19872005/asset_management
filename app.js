var express = require("express");
var app = express();
var http = require('http');


var server = app.listen(3000, function () {
  console.log("Node.js is listening to PORT:" + server.address().port);
});

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

var dinner = {
  "rangeA": {
    "label": "0-10分",
    "result": 20
  },
  "rangeB": {
    "label": "10-20分",
    "result": 35
  },
  "rangeC": {
    "label": "20-30分",
    "result": 70
  },
  "rangeD": {
    "label": "30-40分",
    "result": 80
  },
  "rangeE": {
    "label": "40-50分",
    "result": 40
  },
  "rangeF": {
    "label": "50-60分",
    "result": 30
  },
  "rangeG": {
    "label": "60分以上",
    "result": 10
  }
}

var preference = {
  "questionA": {
    "label": "安心・安全（農薬・添加物）",
    "result": 15
  },
  "questionB": {
    "label": "安心・安全（放射性物質）",
    "result": 30
  },
  "questionC": {
    "label": "おいしさ",
    "result": 45
  },
  "questionD": {
    "label": "価格",
    "result": 20
  },
  "questionE": {
    "label": "鮮度",
    "result": 60
  },
  "questionF": {
    "label": "食材のバリエーション",
    "result": 55
  },
  "questionG": {
    "label": "サービス対応の良さ",
    "result": 35
  }
}

var concern = {
  "questionA": {
    "label": "野菜の鮮度が不安",
    "result": 75
  },
  "questionB": {
    "label": "配送された野菜を使いきれなさそう",
    "result": 100
  },
  "questionC": {
    "label": "野菜のバリエーションが少ない",
    "result": 90
  },
  "questionD": {
    "label": "価格が高い",
    "result": 35
  },
  "questionE": {
    "label": "家族の好き嫌いに対応できない",
    "result": 50
  },
  "questionF": {
    "label": "配送を受け取れるか不安",
    "result": 65
  },
  "questionG": {
    "label": "特になし",
    "result": 80
  }
}

var experience = {
  "questionA": {
    "label": "はじめてサンプル購入をした",
    "result": 50
  },
  "questionB": {
    "label": "他社のサンプル購入をしたことがある",
    "result": 80
  },
  "questionC": {
    "label": "今まで他社の定期便を利用していた",
    "result": 30
  }
}

var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(bodyParser.json());


app.post('/survey', function (req, res) {
  // リクエストボディを出力
  console.log(req.body);
  var intdinner = req.body.dinner
  var intpreference = req.body.preference
  var intconcern = req.body.concern
  var intexperience = req.body.experience
  console.log(req.body.dinner);
  console.log(req.body.preference);
  console.log(req.body.concern);
  console.log(req.body.experience);

  //ここからPOST
  ////////////////////////////////////////////////////////

  switch (intdinner) {
    case '0':
      dinner.rangeA.result++;
      break
    case '1':
      dinner.rangeB.result++;
      console.log("testtesttest");
      break
    case '2':
      dinner.rangeC.result++
      break
    case '3':
      dinner.rangeD.result++;
      break
    case '4':
      dinner.rangeE.result++;
      break
    case '5':
      dinner.rangeF.result++
      break
    case '6':
      dinner.rangeG.result++
      break
    default:
      console.log('error dinner');
      break
  }

  //////////////////////////////////////////////////////////////////////

  switch (intpreference) {
    case '0':
      preference.questionA.result++;
      break
    case '1':
      preference.questionB.result++;
      break
    case '2':
      preference.questionC.result++;
      break
    case '3':
      preference.questionD.result++;
      break
    case '4':
      preference.questionE.result++;
      break
    case '5':
      preference.questionF.result++;
      break
    case '6':
      preference.questionG.result++
      break
    default:
      console.log('error preference')
      break
  }

  //////////////////////////////////////////////////////////////
  switch (intconcern) {
    case '0':
      concern.questionA.result++;
      break
    case '1':
      concern.questionB.result++;
      break;
    case '2':
      concern.questionC.result++;
      break
    case '3':
      concern.questionD.result++;
      break;
    case '4':
      concern.questionE.result++;
      break
    case '5':
      concern.questionF.result++;
      break
    case '6':
      concern.questionG.result++;
      break
    default:
      console.log('error concern')
      break
  }

  ///////////////////////////////////////////////////////////////
  switch (intexperience) {
    case '0':
      experience.questionA.result++;
      break
    case '1':
      experience.questionB.result++;
      break
    case '2':
      experience.questionC.result++;
      break
    default:
      console.log('error experience');
      break
  }

  res.send({
    message: 'success'
  });
})

//ここまでPOST///////////////////////////////////////////////////////////////////////



//ここからGET
///////////////////////////////////////////////////////////////
app.get("/diagram/dinner", function (req, res, next) {
  res.json(dinner);
});

app.get("/diagram/preference", function (req, res, next) {
  res.json(preference);
});

app.get("/diagram/concern", function (req, res, next) {
  res.json(concern);
});


app.get("/diagram/experience", function (req, res, next) {
  res.json(experience);
});
///////////////////////////////////////////////////////////////



// 特定のリクエスト時にクロスドメイン通信許可する
// app.get('/hoge', function (request, response) {
//   response.header('Access-Control-Allow-Origin', '*');
//   response.json(hoge);
// });


// var dinner = [
// {
//   "rangeA": {
//     "label": "0-10分",
//     "result": 20
//   },
//   "rangeB": {
//     "label": "10-20分",
//     "result": 35
//   },
//   "rangeC": {
//     "label": "20-30分",
//     "result": 70
//   },
//   "rangeD": {
//     "label": "30-40分",
//     "result": 80
//   },
//   "rangeE": {
//     "label": "40-50分",
//     "result": 40
//   },
//   "rangeF": {
//     "label": "50-60分",
//     "result": 30
//   },
//   "rangeG": {
//     "label": "60分以上",
//     "result": 10
//   }
// }
// ]

// var preference = [
// {
//   "questionA": {
//     "label": "安心・安全（農薬・添加物）",
//     "result": 15
//   },
//   "questionB": {
//     "label": "安心・安全（放射性物質）",
//     "result": 30
//   },
//   "questionC": {
//     "label": "おいしさ",
//     "result": 45
//   },
//   "questionD": {
//     "label": "価格",
//     "result": 20
//   },
//   "questionE": {
//     "label": "鮮度",
//     "result": 60
//   },
//   "questionF": {
//     "label": "食材のバリエーション",
//     "result": 55
//   },
//   "questionG": {
//     "label": "サービス対応の良さ",
//     "result": 35
//   }
// }
// ]

// var concern = [
// {
//   "questionA": {
//     "label": "野菜の鮮度が不安",
//     "result": 75
//   },
//   "questionB": {
//     "label": "配送された野菜を使いきれなさそう",
//     "result": 100
//   },
//   "questionC": {
//     "label": "野菜のバリエーションが少ない",
//     "result": 90
//   },
//   "questionD": {
//     "label": "価格が高い",
//     "result": 35
//   },
//   "questionE": {
//     "label": "家族の好き嫌いに対応できない",
//     "result": 50
//   },
//   "questionF": {
//     "label": "配送を受け取れるか不安",
//     "result": 65
//   },
//   "questionG": {
//     "label": "特になし",
//     "result": 80
//   }
// }
// ]

// var experience = [
// {
//   "questionA": {
//     "label": "はじめてサンプル購入をした",
//     "result": 50
//   },
//   "questionB": {
//     "label": "他社のサンプル購入をしたことがある",
//     "result": 80
//   },
//   "questionC": {
//     "label": "今まで他社の定期便を利用していた",
//     "result": 30
//   }
// }
// ]

// app.get("/diagram/dinner", function(req, res, next){
// 	res.json(dinner);
// });

// app.get("/diagram/preference", function(req, res, next){
// 	res.json(preference);
// });

// app.get("/diagram/concern", function(req, res, next){
// 	res.json(concern);
// });

// app.get("/diagram/experience", function(req, res, next){
// 	res.json(experience);
// });